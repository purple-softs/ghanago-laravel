<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// TODO: remove this route
Route::get('/test', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

/* ======== buyer routes ===========*/
Route::get('/user/buyer/dashboard', 'BuyerController@dashboard')->name('buyer-dashboard');

Route::get('/user/buyer/dashboard/profile', 'BuyerController@profile')->name('buyer-profile');

Route::post('/user/buyer/dashboard/profile/update', 'BuyerController@updateProfile')->name('buyer-update-profile');

Route::get('/user/buyer/dashboard/delivery/unconfirmed', 'BuyerController@unconfirmedDelivery')->name('buyer-unconfirmed-delivery');

Route::get('/user/buyer/dashboard/delivery/{id}/confirm', 'BuyerController@confirmDelivery')->name('buyer-confirm-delivery');
/* ======== END: Buyer routes ===========*/

/* ======== Seller routes ===========*/
Route::get('/user/seller/dashboard', 'SellerController@index')->name('seller-dashboard');

Route::match(
    ['get', 'post'],
    '/user/seller/product/new',
    'SellerController@newProduct'
)->name('new-product');

Route::match(['get', 'post'], '/products/{id}/edit', 'SellerController@editProduct')->name('edit-product');

Route::get('/user/seller/products', 'SellerController@products')->name('products');

Route::get('/user/seller/orders/new', 'SellerController@newOrders')->name('new-orders');

Route::get('/user/seller/orders/new/{id}/details', 'SellerController@orderDetails')->name('orders-details');

Route::get('/user/seller/dashboard/profile', 'SellerController@profile')->name('seller-profile');

Route::post('/user/buyer/dashboard/profile/update', 'SellerController@updateProfile')->name('seller-update-profile');
/* ======== END: Seller routes ===========*/


/* ======== Product routes ===========*/
Route::get('/products', 'ProductController@index')->name('all-products');

Route::get('/products/{id}/details', 'ProductController@productDetails')->name('product-details');

Route::post('/products/{id}/purchase', 'ProductController@purchaseProduct')->name('purchase-product')->middleware('auth');
/* ======== END: Product routes ===========*/
