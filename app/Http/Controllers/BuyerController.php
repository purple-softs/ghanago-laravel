<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\BuyerProfile;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class BuyerController extends Controller {
    protected $unconfirmed_delivery;

    /**
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');

        $this->middleware(function ($request, $next) {
            $this->unconfirmed_delivery = Order::where([
                ['user_id', '=', Auth::user()->id],
                ['delivery_confirmed', '=', false],
            ])->count();

            return $next($request);
        });
    }

    public function dashboard() {
        return view('buyer.dashboard', ['unconfirmed_delivery' => $this->unconfirmed_delivery]);
    }

    public function profile() {
        return view('buyer.profile', [
            'unconfirmed_delivery' => $this->unconfirmed_delivery,
            'profile'              => BuyerProfile::where('user_id', '=', Auth::id())->get()->first()
        ]);
    }

    // TODO: validate request data
    public function updateProfile(Request $req) {
        $data = $req->all();
        $resp = BuyerProfile::updateProfile($data);
        if ($resp) {
            return back()->with('success', 'Profile updated successfully');
        }

        return back()->with('error', 'An error occured will processing request');
    }

    public function unconfirmedDelivery() {
        $orders = DB::table('orders')
            ->join('products', function ($join) {
                $join->on('orders.product_id', '=', 'products.id')
                    ->where([
                        ['orders.delivery_confirmed', '=', false],
                        ['orders.user_id', '=', auth()->user()->id],
                    ]);
            })->select('orders.*', 'products.name')->get();

        return view('buyer.unconfirmed_delivery', ['unconfirmed_delivery' => $this->unconfirmed_delivery,
            'orders'                                                      => $orders, 'work' => 'workinng',
        ]);
    }

    public function confirmDelivery($id) {
        echo $id;
        Order::where('id', '=', $id)->update(['delivery_confirmed' => true]);

        return back()->with('success', 'Delivery confirmed successfully');
    }
}
