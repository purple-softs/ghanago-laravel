<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Order;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderPlaced;
use App\Mail\NewOrderSellerAlert;
use App\User;

class ProductController extends Controller {
    // TODO; redirect user to login when he is about to purchase a product

    public function index(Request $request) {
        return view('products', [
            'products'   => Product::orderBy('created_at', 'desc')->paginate(16),
            'categories' => Category::orderBy('name', 'asc')->get()
        ]);
    }

    public function productDetails(Request $request, $id) {
        $product = Product::where('id', '=', $id)->first();

        return view('product_details', [
            'product'    => $product,
            'categories' => Category::orderBy('name', 'asc')->get()
        ]);
    }

    public function purchaseProduct(Request $request, $id) {
        $product    = Product::get()->where('id', '=', $id)->first();
        $qty_orderd = $request->input('quantity_ordered');

        if ($qty_orderd > ($product->quantity - $product->quantity_sold)) {
            return back()->with('error', 'Ordered qunatity exceeds quantity left');
        }

        $total_cost = $product->price_per_unit * $qty_orderd;
        $resp       = Order::new($id, $qty_orderd);
        if ((sizeof($resp) == 2) || ($resp != false)) {
            Mail::to($request->user())->send(new OrderPlaced($resp));
            $user = User::where('id', '=', $product->added_by)->first();
            Mail::to($user->email)->send(new NewOrderSellerAlert($resp));

            return back()->with('success', "Order placed successfully, you'll recieve your orders in 15 days");
        } else {
            return back()->with('error', 'An error occured while processing transaction');
        }

        return view('purchase_product');
    }
}
