<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use App\Models\Category;
use App\Models\SellerProfile;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\User;

class SellerController extends Controller {
    protected $new_orders;

    /**
     * @return void
     */
    public function __construct() {
        // TODO: add a middleware for checking if the
        // user is a seller
        $this->middleware('auth');

        $this->middleware(function ($request, $next) {
            $this->new_orders = DB::table('orders')
                ->join('products', function ($join) {
                    $join->on('orders.product_id', '=', 'products.id')
                        ->where([
                            ['orders.delivery_confirmed', '=', false],
                            ['products.added_by', '=', auth()->user()->id],
                        ]);
                })->count();

            return $next($request);
        });
    }

    public function index() {
        $orders = DB::table('orders')
            ->join('products', function ($join) {
                $join->on('orders.product_id', '=', 'products.id')
                    ->where([
                        ['orders.delivery_confirmed', '=', false],
                        ['products.added_by', '=', auth()->user()->id],
                    ]);
            })->select('orders.*', 'products.name')->get();

        return view('dashboards.seller.index', [
            'new_orders' => $this->new_orders,
            'orders'     => $orders
        ]);
    }

    public function dashboard() {
        return view('seller.dashboard', ['new_orders' => $this->new_orders]);
    }

    public function products() {
        $products = Product::where('added_by', '=', auth()->user()->id)
            ->latest()->get();

        return view('dashboards.seller.products', [
            'products'   => $products,
            'new_orders' => $this->new_orders,
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     */
    public function newProduct(Request $request) {
        if ($request->isMethod('post')) {
            // FIXME: validation not working properly
            // $this->validate($request, [
            //     'name' => ['required', 'string', 'max:100',
            //     ],
            //     'category' => ['required', 'string'],
            //     'quantity' => ['required', 'integer', 'min:1'],
            //     'price' => ['required', 'numeric', 'min:1'],
            //     'long_description' => ['required', 'string', 'min:100'],
            //     'short_description' => ['required', 'string', 'min:50'],
            //     // 'product_image' => ['required', 'file', 'array'],
            // ]);

            $path = $request->file('product_image')->store('public/products');
            $resp = Product::add($request->all(), Storage::url($path));

            if ($resp) {
                return back()->with('success', 'Product has been added successfully');
            }

            return back()->with('error', 'An error occured');
        }

        return view('dashboards.seller.new_product', [
            'new_orders' => $this->new_orders,
            'categories' => Category::all()
        ]);
    }

    // TODO: implement editing of product image(s)
    public function editProduct(Request $request, $id) {
        if ($request->isMethod('post')) {
            $data = $request->all();
            $old_product = Product::where('id', '=', $id)->first();
            if ($data['quantity'] < $old_product->quantity) {
                return back()->with('error', "Product quantity cannot be less than the previous value $old_product->quantity");
            }
            $data['old_quantity'] = $old_product->quantity;
            $resp = Product::updateProduct($id, $data);
            if ($resp) {
                return back()->with('success', 'Product updated successfully');
            }

            return back()->with('error', 'An error occured while processing request');
        }

        return view('dashboards.seller.edit_product', [
            'product'    => Product::where('id', '=', $id)->first(), 'categories' => Category::all(),
            'new_orders' => Order::recentlyOrdered(),
        ]);
    }

    public function newOrders(Request $request) {
        $orders = DB::table('orders')
            ->join('products', function ($join) {
                $join->on('orders.product_id', '=', 'products.id')
                    ->where([
                        ['orders.delivery_confirmed', '=', false],
                        ['products.added_by', '=', auth()->user()->id],
                    ]);
            })->select('orders.*', 'products.name')->get();

        return view('seller.new_orders', ['orders' => $orders, 'new_orders' => $this->new_orders]);
    }

    public function orderDetails(Request $request, $id) {
        $order = Order::where('id', '=', $id)->get()->first();
        $product = Product::where('id', '=', $order->product_id)->get()->first();

        return view('dashboards.seller.order_details', [
            'order'        => $order,
            'product_name' => $product->name, //TODO: remove this
            'product'      => $product,
            'new_orders'   => $this->new_orders,
            'buyer'        => User::where('id', '=', $order->user_id)->get()->first(),
            // TODO: replace with join query. use both Buyer & Seller Profile
            'buyer_profile' => SellerProfile::where('user_id', '=', $order->user_id)->get()->first()
        ]);
    }

    public function profile() {
        return view('dashboards.seller.profile', [
            'new_orders'           => Order::recentlyOrdered(),
            'profile'              => SellerProfile::where('user_id', '=', Auth::id())->get()->first()
        ]);
    }

    // TODO: validate request data
    public function updateProfile(Request $req) {
        $data = $req->all();
        $resp = SellerProfile::updateProfile($data);
        if ($resp) {
            return back()->with('success', 'Profile updated successfully');
        }

        return back()->with('error', 'An error occured will processing request');
    }
}
