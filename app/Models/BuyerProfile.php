<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Illuminate\Support\Facades\DB;

class BuyerProfile extends Model {
    public $table = 'buyer_profiles';

    public $fillable = ['name', 'tin', 'gps_code', 'email', 'phone'];

    public function scopeUpdateProfile($query, $data) {
        $result = DB::transaction(function () use ($data) {
            DB::table($this->table)->where('user_id', '=', Auth::id())->updateTs([
                'tin'     => $data['tin'],
                'phone'   => $data['phone'],
                'gps_code'=> $data['gps_code'],
            ]);

            DB::table('users')->where('id', '=', Auth::id())->updateTs([
                'email'=> $data['email'],
                'name' => $data['name']
            ]);

            return true;
        });

        return $result ? true : false;
    }
}
