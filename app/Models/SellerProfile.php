<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;

class SellerProfile extends Model {
    public $table = 'seller_profiles';

    public $fillable = [
        'user_id', 'phone', 'gps_code',
        'fda_code', 'postal_address', 'image',
        'tin', 'phone'
    ];

    protected $casts = [
        'created_at'=> 'datetime',
        'updated_at'=> 'datetime'
    ];

    public function scopeUpdateProfile($query, $data) {
        $result = DB::transaction(function () use ($data) {
            DB::table($this->table)->where('user_id', '=', Auth::id())->updateTs([
                'tin'     => $data['tin'],
                'phone'   => $data['phone'],
                'gps_code'=> $data['gps_code'],
                'fda_code'=> $data['fda_code'],
            ]);

            DB::table('users')->where('id', '=', Auth::id())->updateTs([
                'email'=> $data['email'],
                'name' => $data['name']
            ]);

            return true;
        });
        echo $result;
        echo $result;
        echo $result;
        echo $result;

        return $result ? true : false;
    }
}
