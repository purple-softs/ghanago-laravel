<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model {
    public $table = 'orders';

    public $fillable = [
        'delivery_confirmed', 'user_id',
        'product_id', 'quantity', 'total_cost',
    ];

    protected $casts = [
        'created_at' => 'datetime',
    ];

    public function order_number($size=9) {
        $alpha_key = '';
        $keys      = range('A', 'Z');

        for ($i = 0; $i < 2; $i++) {
            $alpha_key .= $keys[array_rand($keys)];
        }

        $length = $size - 2;
        $key    = '';
        $keys   = range(0, 9);

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $alpha_key . $key;
    }

    public function scopeNew($query, $product_id, $qty_orderd) {
        $product    = Product::get()->where('id', '=', $product_id)->first();
        $total_cost = round($product->price_per_unit * $qty_orderd, 2);

        $result = DB::transaction(function () use ($product, $total_cost, $qty_orderd) {
            $order_id = DB::table('orders')->insertGetIdTs([
                'product_id'     => $product->id,
                'price_per_unit' => $product->price_per_unit,
                'total_cost'     => $total_cost,
                'quantity'       => $qty_orderd,
                'user_id'        => auth()->user()->id,
                'order_number'   => (string)$this->order_number(),
            ]);

            DB::table('products')
                    ->where('id', '=', $product->id)
                    ->increment('quantity_sold', $qty_orderd);

            DB::table('categories')
                ->where('id', '=', $product->category_id)
                ->decrement('total_products', $qty_orderd);

            return ['order_id' => $order_id, 'product_id' => $product->id];
        });

        return sizeof($result) == 2 ? $result : false;
    }

    public function scopeRecentlyOrdered($query) {
        return  DB::table('orders')
                ->join('products', function ($join) {
                    $join->on('orders.product_id', '=', 'products.id')
                        ->where([
                            ['orders.delivery_confirmed', '=', false],
                            ['products.added_by', '=', auth()->user()->id],
                        ]);
                })->count();
    }
}
