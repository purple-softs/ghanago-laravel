<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model {
    public $table = 'products';

    public $fillable = [
        'name', 'category', 'quantity',
        'long_description', 'short_description',
        'image', 'price', 'added_by',
        'price_per_unit', 'quantity_sold',
        'created_at', 'updated_at',
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    public function scopeAdd($query, $data, $img_url) {
        $result = DB::transaction(function () use ($data, $img_url) {
            DB::table('products')->insertTs([
                'name'                 => $data['name'],
                'category_id'          => $data['category'],
                'price_per_unit'       => $data['price'],
                'short_description'    => $data['short_description'],
                'long_description'     => $data['long_description'],
                'quantity'             => $data['quantity'],
                'image'                => $img_url,
                'added_by'             => auth()->user()->id,
            ]);

            DB::table('categories')->where('id', '=', $data['category'])
                ->increment('total_products', $data['quantity']);

            return true;
        });

        return $result ? true : false;
    }

    public function scopeUpdateProduct($query, $id, $data) {
        $result = DB::transaction(function () use ($id, $data) {
            DB::table('products')->where('id', '=', $id)->updateTs([
                'name'                 => $data['name'],
                'category_id'          => $data['category'],
                'price_per_unit'       => $data['price'],
                'short_description'    => $data['short_description'],
                'long_description'     => $data['long_description'],
                'quantity'             => $data['quantity'],
            ]);

            DB::table('categories')->where('id', '=', $data['category'])
                ->increment('total_products', $data['quantity'] - $data['old_quantity']);

            return true;
        });

        return $result ? true : false;
    }

    public function scopeNew($query) {
        return $query->latest()->simplePaginate(4);
    }

    public function scopeOld($query) {
        return $query->skip(5)->simplePaginate(4);
    }
}
