<?php

namespace App\Mail;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;

class NewOrderSellerAlert extends Mailable {
    use Queueable, SerializesModels;

    public $order;
    public $product;
    public $buyer;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data) {
        $this->order   = Order::where('id', '=', $data['order_id'])->first();
        $this->product = Product::where('id', '=', $data['product_id'])->first();
        // TODO: display user details using this object in the view
        $this->buyer = User::where('id', '=', $this->product->added_by)->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->from('support@ghanago.com')
            ->view('emails.orders.new_order_seller_alert');
        // return $this->view('view.name');
    }
}
