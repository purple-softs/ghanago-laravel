<?php

namespace App\Mail;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderPlaced extends Mailable {
    use Queueable, SerializesModels;

    public $order;
    public $product;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data) {
        $this->order   = Order::where('id', '=', $data['order_id'])->first();
        $this->product = Product::where('id', '=', $data['product_id'])->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->from('support@ghanago.com')
            ->view('emails.orders.placed');
        // return $this->view('view.name');
    }
}
