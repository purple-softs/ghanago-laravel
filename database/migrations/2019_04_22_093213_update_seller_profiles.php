<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSellerProfiles extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('seller_profiles', function (Blueprint $table) {
            $table->string('phone')->nullable($value = true)->change();
            $table->string('gps_code')->nullable($value = true)->change();
            $table->string('fda_code')->nullable($value = true)->change();
            $table->string('image')->nullable($value = true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('seller_profiles', function (Blueprint $table) {
            $table->string('phone');
            $table->string('image');
            $table->string('gps_code');
            $table->string('fda_code');
        });
    }
}
