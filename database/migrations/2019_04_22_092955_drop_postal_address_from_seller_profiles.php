<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropPostalAddressFromSellerProfiles extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('seller_profiles', function (Blueprint $table) {
            $table->dropColumn('postal_address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('seller_profiles', function (Blueprint $table) {
            $table->string('postal_address');
        });
    }
}
