<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBuyerProfilesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('buyer_profiles', function (Blueprint $table) {
            $table->string('phone')->nullable($value = true)->change();
            $table->string('tin')->nullable($value = true)->change();
            $table->string('gps_code')->nullable($value = true)->change();
            $table->string('image')->nullable($value = true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('buyer_profiles', function (Blueprint $table) {
            $table->string('phone')->change();
            $table->string('tin')->change();
            $table->string('phone')->change();
            $table->string('gps_code')->change();
            $table->string('image')->change();
        });
    }
}
