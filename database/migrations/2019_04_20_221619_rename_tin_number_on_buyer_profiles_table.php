<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameTinNumberOnBuyerProfilesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('buyer_profiles', function (Blueprint $table) {
            $table->renameColumn('tin_number', 'tin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('buyer_profiles', function (Blueprint $table) {
            $table->renameColumn('tin', 'tin_number');
        });
    }
}
