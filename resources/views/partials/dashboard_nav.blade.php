<nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
  {{-- TODO: replace with hunberger icon --}}
  <button class="btn btn-primary" id="menu-toggle"><span class="oi oi-chevron-left" title="toggle menu" aria-hidden="true"></span></button>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="">Home <span class="sr-only">(current)</span></a>
      </li>

      <li class='nav-item dropdown'>
          {{-- TODO: set username ot dropdown links --}}
          <a class='nav-link dropdown-toggle' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
              {{ Auth::user()->name }}
          </a>
          <div class='dropdown-menu' aria-labelledby='navbarDropdown'>
              <a class='dropdown-item' href="{{Auth::user()->user_type == "buyer" ? route('buyer-dashboard') : route('seller-dashboard') }}">Dashboard</a>
              <div class='dropdown-divider'></div>
              <a class='dropdown-item' onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();" href='{{ route('logout') }}'>
                  {{ __('Logout') }}
              </a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
          </div>
      </li>

    </ul>
  </div>
</nav>
