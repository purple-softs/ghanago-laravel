<!-- Sidebar -->
<div class="bg-light border-right" id="sidebar-wrapper">
  <div class="sidebar-heading">{{ Auth::user()->name }}</div>

  <div class="list-group list-group-flush">
    <a href="{{ route('seller-dashboard') }}" class="list-group-item list-group-item-action bg-light"><i class="oi oi-dashboard"></i> Dashboard</a>

    <a href="{{ route('new-orders') }}" class="list-group-item list-group-item-action bg-light"><i class="oi oi-dashboard"></i> New Orders
        <span class="badge badge-info">{{ $new_orders }}</span>
        <span class="sr-only">Unconfirmed new orders</span>
    </a>

    {{-- products --}}
    {{-- TODO: add icons to sidebar items --}}
    {{-- <a href="#orders" class="list-group-item list-group-item-action bg-light" data-toggle="collapse">
         <i class="oi oi-chevron-right"></i> Products
       </a>
       <div class="list-group collapse" id="orders">
         <a href="{{ route('new-orders') }}" class="list-group-item sub list-group-item-action bg-light"><i class="oi oi-grid-two-up"></i>  New</a>
         <a href="{{ route('confirmed-orders') }}" class="list-group-item sub list-group-item-action bg-light"><i class="oi oi-pencil"></i> New</a>
       </div> --}}

    {{-- TODO: add icons to sidebar items --}}
    <a href="#product" class="list-group-item list-group-item-action bg-light" data-toggle="collapse">
         <i class="oi oi-chevron-right"></i> Products
       </a>
       <div class="list-group collapse" id="product">
         <a href="{{ route('products') }}" class="list-group-item sub list-group-item-action bg-light"><i class="oi oi-grid-two-up"></i>  All</a>
         <a href="{{ route('new-product') }}" class="list-group-item sub list-group-item-action bg-light"><i class="oi oi-pencil"></i> New</a>
       </div>

      <a href="{{ route('seller-profile') }}" class="list-group-item list-group-item-action bg-light"><i class="oi oi-account"></i> Profile</a>

    {{-- logout--}}
    <a onclick="event.preventDefault(); document.getElementById('logout-form').submit();" href="{{ route('logout') }}" class="list-group-item list-group-item-action bg-light"><i class="oi oi-account-logout"></i> Logout</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
  </div>
</div>
<!-- /#sidebar-wrapper -->
