<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="{{URL::asset('/')}}" target="_top">
    <link rel="stylesheet" href="{{{ URL::asset('bootstrap/css/bootstrap.min.css')}}}" />
    <link href="{{{ URL::asset('open-iconic/font/css/open-iconic-bootstrap.css') }}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{{ URL::asset('css/sidebar.css')}}}">

    @if (!Request::is('*/buyer/*'))
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/seller-dashboard.css') }}">
    @endif

    <style type="text/css">
        {{-- Prevent bootstrap dropdown from overflowing --}}
        body {
            overflow: scroll-y;
            overflow-x: hidden;
        }
    </style>
    @yield('css')
    <title>@yield('title')</title>
</head>
