<!-- Sidebar -->
<div class="bg-light border-right" id="sidebar-wrapper">
  <div class="sidebar-heading">{{ Auth::user()->name }}</div>

  <div class="list-group list-group-flush">
    <a href="{{ route('buyer-dashboard') }}" class="list-group-item list-group-item-action bg-light"><i class="oi oi-dashboard"></i> Dashboard</a>


    <a href="{{ route('buyer-unconfirmed-delivery') }}" class="list-group-item list-group-item-action bg-light">
      <i class="oi oi-dashboard"></i> Unconfirmed Delivery
      <span class="badge badge-info">{{ $unconfirmed_delivery }}</span>
      <span class="sr-only">unconfirmed delivery</span>
    </a>

    {{-- Orders --}}
    {{-- TODO: replace with orders route url --}}
    <a href="{{ route('buyer-dashboard') }}" class="list-group-item list-group-item-action bg-light"><i class="oi oi-dashboard"></i> Orders</a>

    <a href="{{ route('buyer-profile') }}" class="list-group-item list-group-item-action bg-light"><i class="oi oi-account"></i> Profile</a>


    {{-- logout--}}
    <a onclick="event.preventDefault(); document.getElementById('logout-form').submit();" href="{{ route('logout') }}" class="list-group-item list-group-item-action bg-light"><i class="oi oi-account-logout"></i> Logout</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
  </div>
</div>
<!-- /#sidebar-wrapper -->
