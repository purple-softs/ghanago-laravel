@extends('layouts.master')
@section('title', 'GhanaGo | Home')
@section('content')
<section>
    <h4 class="text-center mt-3 mb-3 text-capitalize font-weight-bold">New Products</h4>
    @foreach ($new_products->chunk(4) as $chunk)
    <div class="row">
        @foreach ($chunk as $product)
        <div class="col-md-3 text-justify">
            <img src="{{ $product->image }}" alt="{{ $product->name }} image" class="rounded img-fluid product-image">
            <div class="overlay">
                <a href="{{ route('product-details', ['id'=>$product->id]) }}"><i class="oi oi-eye view stretch-link"></i></a>
            </div>
            <p class="text-center pt-2">
                {{ $product->name }}<br> &#x20B5;{{ $product->price_per_unit }}
            </p>
        </div>
        @endforeach
    </div>
    @endforeach
    <div class="more-products">
        <a href="{{ route('all-products') }}"><button class="btn btn-primary rounded-pill ml-5">more...</button></a>
    </div>

    <h4 class="text-center mt-5 mb-3 text-capitalize font-weight-bold">Our Products</h4>
    @foreach ($old_products->chunk(4) as $chunk)
    <div class="row">
        @foreach ($chunk as $product)
        <div class="col-md-3">
            <img src="{{ $product->image }}" alt="{{ $product->name }} image" class="rounded img-fluid product-image">
            <div class="overlay">
                <a href="{{ route('product-details', ['id'=>$product->id]) }}"><i class="oi oi-eye view stretch-link"></i></a>
            </div>
            <p class="text-center">
                {{ $product->name }}<br> &#x20B5;{{ $product->price_per_unit }}
            </p>
        </div>
        @endforeach
    </div>
    @endforeach
    <div class="more-products">
        <a href="{{ route('all-products') }}"><button class="btn btn-primary rounded-pill ml-5">more...</button></a>
    </div>


@section('footer')
    <div class="row mt-5 mb-5">
        <div class="col-md-4">
            <h3>About Us</h3>
            <p class="text-justify">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Beatae, quasi? Praesentium corporis vitae nemo incidunt
                unde alias? Incidunt reiciendis voluptas error quam natus ea dicta tenetur, ab earum quisquam voluptatem?</p>
        </div>
        <div class="col-md-4">
            <h4>Newsletter</h4>
            <p>Stay update with our latest</p>
            <input type="email" placeholder="enter email">
        </div>
        <div class="col-md-4">
            <h4>Contact Information</h4>
            <p>
                291 South 21st Street,<br> Suite 721 New York NY 10019,<br> +233 342 343 234<br> info@yoursite.com
                <br> yoursite.com
                <br>
            </p>
        </div>
    </div>
@endsection


</section>
@endsection
