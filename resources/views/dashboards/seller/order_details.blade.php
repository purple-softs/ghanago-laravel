@extends('dashboards.layouts.main')
@section('title', "Product Details - $product->name")

@section('nav-product')
<li class="nav-item dropdown active">
@endsection

@section('main')
<section>
    <div class="card">
        <header class="card-header"><i class="fas fa-info-circle"></i> {{ $product->name }} Order Details</header>
        <div class="card-bod">
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-md-4 pt-md-5">
                        <img src="{{ $product->image }}" class="card-img" alt="{{ $product->image }} image">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">{{ $product->name }}</h5>
                            <p class="card-text pl-md-4">{{ $product->long_description }}</p>

                            <hr>

                            <h5 class="card-text font-weight-bold text-center">Order Details</h5>
                            <div class="card-text pl-md-4">
                                <p>Quantity Ordered: {{ $order->quantity }}</p>
                                <p>Price Per Unit (&#x20B5;): {{ $order->price_per_unit }}</p>
                                <strong><p>Total Cost (&#x20B5;): {{ $order->total_cost }}</p></strong>
                                <p>Date and Time ordered: {{ $order->created_at }}</p>
                                <p>Delivery Confirmed: {{ $order->delivery_confirmed ? 'True' : 'False' }}</p>
                            </div>

                            <h5 class="card-text font-weight-bold text-center">Buyer Delivery Details</h5>
                            <div class="card-text pl-md-4">
                                <p>Name: {{ $buyer->name }}</p>
                                <p>Phone: {{ $buyer_profile->phone }}</p>
                                <p>Email: {{ $buyer->email }} </p>
                                <p>Ghana Post Code: {{ $buyer_profile->gps_code }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection


