@extends('dashboards.layouts.main')
@section('title', "Edit Product - $product->name")

@section('nav-product')
<li class="nav-item dropdown active">
@endsection

@section('main')
<section>
    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-edit"></i> Edit Product</div>
        <div class="card-body">
            <p class="text-center"><strong>Product image(s) cannot be edited. We are working on to support this feature</strong></p>
            <div class="row">
                <div class="col-md-8  offset-md-2">
                    @if(Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        {{ Session::get('success') }}
                    </div>
                    @endif @if(Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ Session::get('error') }}
                    </div>
                    @endif

                    <form method="POST" accept="{{ route('edit-product', ['id' => $product->id]) }}" enctype="multipart/form-data">
                        @csrf {{-- TODO: add server-side form validation --}}
                        <div class="form-group row">
                            <label for="category" class="col-sm-3 col-form-label">Category</label>
                            <div class="col-sm-9">
                                <select name="category" id="category" class="form-control" required>
                                    <option value="">select a category</option>
                                    @foreach ($categories as $category)
                                        @if ($product->category_id == $category->id)
                                            <option value="{{ $category->id }}" selected>{{ ucwords($category->name) }}</option>
                                        @else
                                            <option value="{{ $category->id }}">{{ ucwords($category->name) }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label">Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" value="{{ $product->name }}" name="name" id="name" placeholder="eg. Hybrid Maize Seed"
                                    required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="quantity" class="col-sm-3 col-form-label">Quantity</label>
                            <div class="col-sm-9">
                                <input type="number" min="{{ $product->quantity }}" step="1" class="form-control" value="{{ $product->quantity }}" name="quantity"
                                    id="quantity" placeholder="eg. 100" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="price" class="col-sm-3 col-form-label">Price Per Unit (&#x20B5;)</label>
                            <div class="col-sm-9">
                                <input type="number" value="{{ $product->price_per_unit }}" min="0.01" step="0.01" class="form-control" name="price" id="price"
                                    placeholder="eg. &#x20B5;150" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="short_description" class="col-sm-3 col-form-label">Short Description </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="short_description" rows="2" placeholder="at maximum 200 words" required>{{ $product->short_description }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="long_description" class="col-sm-3 col-form-label">Long Description </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="long_description" rows="4" required>{{ $product->long_description }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-4 offset-sm-4">
                                <button type="submit" class="btn btn-primary">Edit Product</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
