@extends('dashboards.layouts.main')
@section('title', Auth::user()->name . ' - Dashboard')

@section('icon-cards')
    @include('dashboards.layouts.icon_cards_seller')
@endsection

@section('nav-dashboard')
<li class="nav-item active">
    <a class="nav-link" href="{{ route('seller-dashboard') }}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span>
    </a>
</li>
@endsection

@section('main')
<div class="card mb-3" id="new-orders">
    <div class="card-header">
        <i class="fas fa-table"></i> New Orders</div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" >
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Total Cost(&#x20B5;)</th>
                        <th>Quantity</th>
                        <th>Date</th>
                        <th>View</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Total Cost(&#x20B5;)</th>
                        <th>Quantity</th>
                        <th>Date</th>
                        <th>View</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($orders as $order)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{ $order->name }}</td>
                            <td>{{ $order->total_cost }}</td>
                            <td>{{ $order->quantity }}</td>
                            <td>{{ $order->created_at}}</td>
                            {{-- TODO: add icon --}}
                            <td><a href="{{ route('orders-details', ['id'=>$order->id]) }}">Details</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer small text-muted">Updated now</div>
</div>
@endsection
