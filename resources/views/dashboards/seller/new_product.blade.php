@extends('dashboards.layouts.main')

@section('title', "Dashboard - Add New Product")

@section('nav-product')
<li class="nav-item dropdown active">
@endsection

@section('main')
<section>
    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-plus-circle"></i> Add Product</div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-8  offset-md-2">
                    @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                    @endif

                    <form method="POST" accept="{{ route('new-product') }}" enctype="multipart/form-data">
                        @csrf {{-- TODO: add servers-die form validation --}}
                        <div class="form-group row">
                            <label for="category" class="col-sm-3 col-form-label">Category</label>
                            <div class="col-sm-9">
                                <select name="category" id="category" class="form-control" required>
                                            <option value="">select a category</option>
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id }}">{{ ucwords($category->name) }}</option>
                                            @endforeach
                                        </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label">Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="name" id="name" placeholder="eg. Hybrid Maize Seed" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="quantity" class="col-sm-3 col-form-label">Quantity</label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" name="quantity" id="quantity" placeholder="eg. 100" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="price" class="col-sm-3 col-form-label">Price Per Unit (&#x20B5;)</label>
                            <div class="col-sm-9">
                                <input type="number" min="0.01" step="0.01" class="form-control" name="price" id="price" placeholder="eg. &#x20B5;150" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="short_description" class="col-sm-3 col-form-label">Short Description </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="short_description" rows="2" placeholder="at maximum 200 words" required></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="long_description" class="col-sm-3 col-form-label">Long Description </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="long_description" rows="4" required></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label" for="product-image">Product Image</label> {{-- TODO: replace with
                            a file picker --}}
                            <div class="col-sm-9">
                                <input type="file" class="form-control-file" id="product-image" name='product_image' required>
                                <small>Only 1 image upload is supported now</small>
                            </div>

                            @if ($errors->has('product_image'))
                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('product_image') }}</strong>
                                        </span> @endif
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-4 offset-sm-4">
                                <button type="submit" class="btn btn-primary">Add Product</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
