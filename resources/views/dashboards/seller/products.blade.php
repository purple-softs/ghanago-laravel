@extends('dashboards.layouts.main')
@section('title', "Products - Dashboard")
@section('nav-product')
<li class="nav-item dropdown active">
@endsection

@section('main')
    <section>
        <div class="card mb-3" id="new-orders">
            <div class="card-header">
                <i class="fas fa-list-alt"></i> All Products</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Price (&#x20B5;)</th>
                                <th scope="col">Qty</th>
                                <th scope="col">Qty Sold</th>
                                <th scope="col">Qty Left</th>
                                <th scope="col">Edit</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Price (&#x20B5;)</th>
                                <th scope="col">Qty</th>
                                <th scope="col">Qty Sold</th>
                                <th scope="col">Qty Left</th>
                                <th scope="col">Edit</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($products as $product)
                            <tr>
                                <th scope="row">{{ $loop->index + 1}}</th>
                                <td>{{ $product->name }}</td>
                                <td class="text-center">{{ $product->price_per_unit }}</td>
                                <td class="text-center">{{ $product->quantity }}</td>
                                <td class="text-center">{{ $product->quantity_sold }}</td>
                                <td class="text-center">{{ $product->quantity - $product->quantity_sold }}</td>
                                <td class="text-center"><a href="{{ route('edit-product', ['id' => $product->id]) }}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
