<!-- Sidebar -->
<ul class="sidebar navbar-nav">
  @section('nav-dashboard')
    <li class="nav-item">
        <a class="nav-link" href="{{ route('seller-dashboard') }}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span>
        </a>
    </li>
    @show

    @section('nav-product')
    <li class="nav-item dropdown">
    @show
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
          <i class="fas fa-fw fa-shopping-basket"></i>
          <span>Products</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            {{-- <h6 class="dropdown-header">Login Screens:</h6> --}}
            <a class="dropdown-item" href="{{ route('products') }}">
              <i class="fas fa-fw fa-list-alt"></i>
              <span>All</span>
            </a>
            <a class="dropdown-item" href="{{ route('new-product') }}">
              <i class="fas fa-fw fa-plus-circle"></i>
              <span>Add</span>
            </a>
        </div>
    </li>

    @section('nav-profile')
    <li class="nav-item">
        <a class="nav-link" href="{{ route('seller-profile') }}">
          <i class="fas fa-fw fa-user"></i>
          <span>Profile</span></a>
    </li>
    @show

    <li class="nav-item">
        <a class="nav-link" href="{{ route('logout') }}">
          <i class="fas fa-fw fa-sign-out-alt"></i>
          <span>Logout</span></a>
    </li>
</ul>
