@extends('layouts.dashboard_master')

@section('title', "Dashboard - Unconfirmed Delivery")

@section('content')
<main style="padding-top: 90px;">
    <article>
        <section>
            <div class="row">
                <div class="col-md-10 offset-md-1 table-responsive-sm">
                    @if(Session::has('success'))
                       <div class="alert alert-success">
                         {{ Session::get('success') }}
                       </div>
                    @endif
                    <table class="table table-hover table-bordered">
                      <thead class="text-center">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Name</th>
                          <th scope="col">Total Cost(&#x20B5;)</th>
                          <th scope="col">Qauntity</th>
                          <th scope="col">Date Ordered</th>
                          {{-- TODO: add icons to these rows --}}
                          <th scope="col">View Details</th>
                          <th scope="col">Confirm Delivery</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($orders as $order)
                            <tr>
                              <th scope="row">{{ $loop->index + 1}}</th>
                              <td>{{ $order->name }}</td>
                              <td class="text-center">{{ $order->total_cost }}</td>
                              <td>{{ $order->quantity }}</td>
                              <td>{{ $order->created_at }}</td>
                              <td>View detials</td>
                              <td><a href="{{ route('buyer-confirm-delivery', ['id'=>$order->id]) }}">Confirm</a></td>
                            </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div>
            </div>
        </section>
    </article>
</main>
@endsection
