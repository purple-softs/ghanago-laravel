@extends('layouts.dashboard_master')
@section('title', "Dashboard - Profile")
@section('content')
<main>
    <article>
        <div class="card">
            <div class="card-header">
                Update Profile
            </div>

            <div class="card-body">

                <section>
                    <div class="row">
                        <div class="col-md-8 offset-md-1">
                            <h5 class="mt-2 pb-3">Personal Details</h5>

                            @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                            @endif @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{ Session::get('error') }}
                            </div>
                            @endif

                            <form method="POST" action="{{ route('buyer-update-profile') }}" name="update-profile" class="ml-md-5">

                                @csrf

                                <div class="form-group row">
                                    <label for="name" class="col-sm-2 col-form-label">Full Name</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="name" name="name" placeholder="eg. John Doe" value="{{ Auth::user()->name}}"
                                            required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-7">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="eg. user@example.com" value="{{ Auth::user()->email }}"
                                            reqired>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="phone" class="col-sm-2 col-form-label">Phone</label>
                                    <div class="col-sm-7">
                                        <input type="tel" class="form-control" id="phone" name="phone" placeholder="eg. 0240845219" value="{{ $profile->phone }}"
                                            reqired>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="tin" class="col-sm-2 col-form-label">TIN</label>
                                    <div class="col-sm-7">
                                        <input type="tel" class="form-control" id="tin" name="tin" placeholder="eg. XXX-XX-XXXX" value="{{ $profile->tin }}"
                                            reqired>
                                        <small id="tinHelp" class="form-text text-muted">Your Tax Identification NUmber (TIN)</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="gps_code" class="col-sm-2 col-form-label">GPS Code</label>
                                    <div class="col-sm-7">
                                        <input type="tel" class="form-control" id="gps_code" name="gps_code" placeholder="eg. AK-039-5028" value="{{ $profile->gps_code }}"
                                            reqired>
                                        <small id="gps_code_help" class="form-text text-muted">Your Ghana Post GPS Code. Don't have one? Visit <a href="https://www.ghanapostgps.com/">Ghana Post</a> for a guide on how to get one.</small>
                                    </div>
                                </div>
                                <div class="form-group row text-center pt-3">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Update Profile</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </section>
                <section>

                </section>
            </div>
        </div>
    </article>
</main>
@endsection
