@extends('layouts.products')
@section('title', 'GhanaGo - Products')
@section('content')
@section('products')
<section>
    @foreach ($products->chunk(4) as $chunk)
    <div class="row">
        @foreach ($chunk as $product)
        <div class="col-md-3 text-justify">
            <img src="{{ $product->image }}" alt="{{ $product->name }} image" class="rounded img-fluid item-image">
            <div class="overlay">
                <a href="{{ route('product-details', ['id'=>$product->id]) }}"><i class="oi oi-eye view stretch-link"></i></a>
            </div>
            <p class="text-center mt-2">
                {{ $product->name }} <br>
                &#x20B5;{{ $product->price_per_unit }}
            </p>
        </div>
        @endforeach
    </div>
    @endforeach {{ $products->links() }}
</section>
@endsection
