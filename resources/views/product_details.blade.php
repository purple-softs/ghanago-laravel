@extends('layouts.products')
@section('title', $product->name . " - Details")
@section('product-name') {{-- TODO: add link
for the active breakcrumb--}}
<li class="breadcrumb-item active" aria-current="page">{{ $product->name }}</li>
@endsection

@section('products')
<section>
    <div class="card mb-3">
        <div class="row no-gutters">
            <div class="col-md-4">
                <img src="{{ $product->image }}" class="card-img" alt="{{ $product->image }} image">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title">{{ $product->name }}</h5>
                    <p class="card-text">{{ $product->long_description }}</p>

                    <p class="card-text font-weight-bold"><span class="pr-1">Price Per Unit:</span> &#x20B5;<span id="price">{{ $product->price_per_unit }}</span></p>
                    <p class="card-text font-weight-bold"><span class="pr-4">Quantity Left:</span> {{ $product->quantity - $product->quantity_sold }}</p>

                    {{-- TODO: replace with buy url --}}
                    <hr>
                    <form action="{{ route('purchase-product', ['id'=>$product->id]) }}" method="POST">
                        @csrf
                        <div class="form-row">
                            <div class="col-sm-8 col-md-5 pr-3">
                                <input class="form-control mb-3" type="number" id="order-qty" name="quantity_ordered" min="1" step="1" max="{{ $product->quantity - $product->quantity_sold }}"
                                    placeholder="quantity to order" required>
                            </div>
                            <div class="col">
                                <p class="text-left mt-1">&#x20B5;<span id="result">0.00</span></p>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col">
                                @if(Session::has('success'))
                                <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                </div>
                                @endif
                                @if(Session::has('error'))
                                <div class="alert alert-danger" role="alert">
                                    {{ Session::get('error') }}
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 text-center">
                                <input type="submit" name="submit" class="btn btn-primary" value="Order Product">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>
    $(document).ready(function(){
        $("#order-qty").bind("change paste keyup", function(){
            var result = parseFloat($("#price").text()) * parseInt($("#order-qty").val());
            var value = (String(result) == "NaN" ? 0.00: result)
            var rounded_value = Number(Math.round(value+'e2')+'e-2')

            $("#result").text(rounded_value.toFixed(2));
        });
    });

</script>
@endsection
