<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('partials.header')
    <body>
     <div class="d-flex" id="wrapper">

          @if (Request::is('*/buyer/*'))
            @include('partials.buyer_sidebar')
          @else
            @include('partials.seller_sidebar')
          @endif


        <!-- Page Content -->
        <div id="page-content-wrapper">

            @include('partials.dashboard_nav')

          <div class="container-fluid mt-2">
            @yield('content')
          </div>

        </div>
        <!-- /#page-content-wrapper -->

      </div>
      <!-- /#wrapper -->

    <script type="text/javascript" src="{{{ URL::asset('js/jquery-3.3.1.min.js') }}}"></script>
    <script type="text/javascript" src="{{{ URL::asset('bootstrap/js/bootstrap.min.js') }}}"></script>
    <script>
    $(document).ready(function () {
      $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");

        $('span', this)
            .toggleClass('io oi-chevron-left')
            .toggleClass('io oi-chevron-right')
      });

      $(function() {

        $('.list-group-item').on('click', function() {
          $('.oi', this)
            .toggleClass('oi-chevron-right')
            .toggleClass('oi-chevron-bottom');
        });

      });
    });
    </script>

    @yield('js')

    </body>
</html>
