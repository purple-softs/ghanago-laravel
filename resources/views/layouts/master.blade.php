<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="{{URL::asset('/')}}" target="_top">
    <link rel="stylesheet" href="{{{ URL::asset('bootstrap/css/bootstrap.min.css') }}}" />
    <link href="{{{ URL::asset('open-iconic/font/css/open-iconic-bootstrap.css') }}}" rel="stylesheet">
    <link rel="stylesheet" href="{{{ URL::asset('css/main.css') }}}">
    <style type="text/css">
        {
                {
                -- Prevent bootstrap dropdown from overflowing --
            }
        }

        body {
            overflow: scroll-y;
            overflow-x: hidden;
        }
    </style>

    @yield('css')
    <title>@yield('title')</title>
</head>

<body>


@section('navbar')
    <nav class="navbar navbar-expand-lg navbar-light bg-light px-md-5">
        <a class="navbar-brand" href="#">GhanaGo</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Store</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
                @else
                <li class='nav-item dropdown'>
                    {{-- TODO: set username ot dropdown links --}}
                    <a class='nav-link dropdown-toggle' href='#' id='navbarDropdown' role='button' data-toggle='dropdown' aria-haspopup='true'
                        aria-expanded='false'>
                                {{ Auth::user()->name }}
                            </a>
                    <div class='dropdown-menu' aria-labelledby='navbarDropdown'>
                        <a class='dropdown-item' href="{{Auth::user()->user_type == "buyer" ? route('buyer-dashboard') : route('seller-dashboard') }}">Dashboard</a>
                        <div class='dropdown-divider'></div>
                        <a class='dropdown-item' onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();" href='{{ route('logout') }}'>
                                    {{ __('Logout') }}
                                </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
                @endguest
            </ul>
        </div>
    </nav>
    @show

    <main class="container">
        @yield('content')

        @section('footer')
            <footer>&copy;2020 GhanaGo Inc.</footer>
        @show
    </main>
    {{--
    @include('partials.header')
    <div class="main-container" id="main-container">
    @include('partials.sidebar')
        <div class="main-content">
            <div class="main-content-inner">
                <div class="breadcrumbs" id="breadcrumbs">
                </div>
                @yield('content')
            </div>
        </div>
    @include('partials.footer')
    </div> --}}


</body>
<script type="text/javascript" src="{{{ URL::asset('js/jquery-3.3.1.min.js') }}}"></script>
<script type="text/javascript" src="{{{ URL::asset('bootstrap/js/bootstrap.min.js') }}}"></script>
@yield('js')
</body>

</html>
