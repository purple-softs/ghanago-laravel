@extends('layouts.master')
@section('title', 'GhanaGo - Products')
@section('content')
<section class="mt-3 mt-md-5">
    <div class="row">
        @section('categories')
        <div class="col-md-2">
            <aside class="categories">
                <h4 id="head">Categories</h4>
                <ul id="items">
                    @foreach ($categories as $category)
                    <li class="text-capitalize"><a href="{{ $category->id }}">{{ $category->name }} ({{ $category->total_products }})</a></li>
                    @endforeach
                </ul>
            </aside>
        </div>
        @show

        <div class="col-md-10">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    {{-- TODO: add new route for displaying categories based on type --}}
                    {{-- @endpush --}}
                    <li class="breadcrumb-item"><a href="#">Category Name</a></li>
                    @yield('product-name')
                </ol>
            </nav>

            @stack('breadcrumb')
            @yield('products')
        </div>
    </div>
</section>
@endsection
