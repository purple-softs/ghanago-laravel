<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>New Product Order</title>
</head>
<body>
    <h4>New Product Order</h4>

    <p>
       New order have been placed for your product {{ $product->name }}<br>
       Below is the details of the order.
    </p>

    <h5>Order Details</h5>
    <p>
        Product Name: {{ $product->name }}<br>
        Price Per Unit: {{ $product->price_per_unit }}  <br>
        Quantity Orderd: {{ $product->quantity_ordered }}  <br>
        Total Cost: &#x20B5;{{ $order->total_cost }} <br>
        Order Number: {{ $order->order_number }} <br>

        <!-- TODO: add the buyer postal address here -->
    </p>

    <p>Thank you for working with us</p>
</body>
</html>
