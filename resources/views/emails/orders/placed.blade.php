<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Product Order</title>
</head>
<body>
    <h4>Order Placement</h4>

    <p>
        Your order for {{ $product->name }} has been received successfully.<br>
        You'll received your product in {working days}.
    </p>

    <h5>Order Details</h5>
    <p>
        Product Name: {{ $product->name }}<br>
        Price Per Unit: {{ $product->price_per_unit }}  <br>
        Quantity Orderd: {{ $product->quantity_ordered }}  <br>
        Total Cost: &#x20B5;{{ $order->total_cost }} <br>
        Order Number: {{ $order->order_number }} <br>
    </p>

    <p>Thank you for working with us</p>
</body>
</html>
